
from keygen import *
from addressgen import CryptoWallet

class Wallet:
    def createWallet(self):
        kg = KeyGenerator()
        kg.seed_input('')
        key = kg.generate_key()
        address = CryptoWallet.generate_address(key)
        public = CryptoWallet.publickeyval(key).decode()
        return [{"privateKey":key,
                 "publickey":public,
                 "address":address}]

    def checkAddress(self,key):
        try:
            address = CryptoWallet.generate_address(key)
            public = CryptoWallet.publickeyval(key).decode()
            return [{"privateKey":key,
                     "publickey":public,
                     "address":address}]
        except:
            return "Invalid Private Key"    

    def signWallet(self,key):
        try:
            address = CryptoWallet.generate_address(key)
            return address
        except:
            return "Failed" 




wall = Wallet()
#add = wall.createWallet() 
#print(add)       

chq = wall.checkAddress("fbf0b8ebfcc1876ddf9e9fbecb084eda4f466070a49a0f7198702549127ff9dc") #d0
#print(chq)
#chq = wall.checkAddress("8cd61a17a32e78a5cf2ac109b4f3846f8b23d13159bfc538aa4b765d2569978a") #66
#print(chq)

#chq = wall.signWallet("8cd61a17a32e78a5cf2ac109b4f3846f8b23d13159bfc538aa4b765d2569978a")
#print(chq)



